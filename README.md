# Asciidoctor antora-kref Extension
:version: 0.0.1

`@djencks/asciidoctor-antora-kref` provides an Asciidoctor.js extension.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-antora-kref/-/blob/master/README.adoc.

## Installation

Available soon through npm as @djencks/asciidoctor-antora-kref.

Currently available through a line like this in `package.json`:


    "@djencks/asciidoctor-antora-kref": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-asciidoctor-antora-kref-v0.0.1.tgz",


The project git repository is https://gitlab.com/djencks/asciidoctor-antora-kref

## Usage in Antora

This extension can only be used with Antora.

see https://gitlab.com/djencks/asciidoctor-antora-kref/-/blob/master/README.adoc

## Antora Example project

An example project showing some uses of this extension is under extensions/antora-kref-extension in `https://gitlab.com/djencks/simple-examples`.
