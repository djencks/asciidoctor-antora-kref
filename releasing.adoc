= How to release to npm
:version: 0.0.1
:repo: asciidoctor-antora-kref
:bundle: djencks-asciidoctor-antora-kref
:url: https://gitlab.com/djencks/{repo}/-/jobs/artifacts/master/raw/{bundle}-v{version}.tgz?job=bundle-stable


* Update this file, package.json, and the two README files with the new version.
* Push to master at gitlab
* When the CI completes the bundle to release should be at:
{url}

* Run
npm publish --access public {url} --dry-run

* Run
npm publish --access public {url}
