/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const antoraKref = require('./../lib/asciidoctor-antora-kref')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

const seed = [
  {
    version: '4.5',
    family: 'page',
    relative: 'page-a.adoc',
    contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
  },
  {
    version: '4.5',
    family: 'page',
    relative: 'kref-1.adoc',
    contents: `

--
kref:page-a.adoc#frag[]
--
`,
  },
]
describe('antora-kref tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  function prepareConfig (seed, src) {
    if (!Array.isArray(seed)) seed = [seed]
    const config = {
      contentCatalog: mockContentCatalog(seed),
      config: { attributes: {} },
    }
    const file = config.contentCatalog.getById(src)
    config.file = file
    return config
  }

  function f (config) {
    const registry = antoraKref.register(asciidoctor.Extensions.create(), config)
    return asciidoctor.load(config.file.contents.toString(), { extension_registry: registry })
  }

  it('basic creole test', () => {
    const doc = f(prepareConfig(seed, {
      version: '4.5',
      component: 'component-a',
      module: 'module-a',
      family: 'page',
      relative: 'kref-1.adoc',
    }))
    const html = doc.convert()
    expect(html).to.equal(`<div class="openblock">
<div class="content">
<div class="paragraph">
<p>[[page-a.html#frag frag]]</p>
</div>
</div>
</div>`)
  })
})
